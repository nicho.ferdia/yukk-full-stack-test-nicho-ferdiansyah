<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Transaction;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $records = Transaction::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(5);
        $balance = Balance::where('user_id', auth()->user()->id)->first();
        return view('home', compact('records', 'balance'));
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        session()->put('query', $query);
        session()->put('filter', null);
        $records = Transaction::where('desc', 'like', "%$query%")
            ->where('user_id', auth()->user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(5);
        $balance = Balance::where('user_id', auth()->user()->id)->first();

        return view('home', compact('records', 'balance'));
    }

    public function filter($type = null)
    {
        session()->put('filter', $type);
        session()->put('query', null);

        if ($type == null) {
            $records = Transaction::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(5);
        } else {
            $records = Transaction::where('type', $type)->where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(5);
        }

        $balance = Balance::where('user_id', auth()->user()->id)->first();

        return view('home', compact('records', 'balance'));
    }
}
