<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Session;

class TransactionController extends Controller
{
    public function transaction()
    {
        return view('transaction');
    }

    public function actiontransaction(Request $request)
    {
        Transaction::create([
            'user_id' => auth()->user()->id,
            'amount' => $request->amount,
            'type' => $request->type,
            'desc' => $request->desc,
        ]);

        if ($request->type == 'topup') {
            Balance::where('user_id', auth()->user()->id)->increment(
                'amount',
                $request->amount,
            );
        }
        if ($request->type == 'transaction') {
            Balance::where('user_id', auth()->user()->id)->decrement(
                'amount',
                $request->amount,
            );
        }

        Session::flash('message', 'Transaction Created.');
        return redirect('/');
    }
}
