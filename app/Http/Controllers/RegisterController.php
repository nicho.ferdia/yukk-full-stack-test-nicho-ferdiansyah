<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Session;

class RegisterController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function actionregister(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        Balance::create([
            'user_id' => $user->id,
            'amount' => 0,
        ]);

        Session::flash('message', 'Register Berhasil. Silahkan Login menggunakan email dan password.');
        return redirect('register');
    }
}
