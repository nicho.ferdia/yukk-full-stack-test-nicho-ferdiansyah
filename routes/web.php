<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Protected
Route::get('/', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('transaction', [TransactionController::class, 'transaction'])->name('transaction');
Route::post('transaction/action', [TransactionController::class, 'actiontransaction'])->name('actiontransaction');

Route::get('/search', [HomeController::class, 'search']);
Route::get('/filter/{type?}', [HomeController::class, 'filter']);

// Login & Logout
Route::get('login', [LoginController::class, 'login'])->name('login');
Route::post('login', [LoginController::class, 'actionlogin'])->name('actionlogin');
Route::get('logout', [LoginController::class, 'actionlogout'])->name('logout');

// Register
Route::get('register', [RegisterController::class, 'register'])->name('register');
Route::post('register/action', [RegisterController::class, 'actionregister'])->name('actionregister');
