@extends('layout')

@section('konten')
    <div class="container-xl px-4 mt-4 mb-4">
        <h4>Selamat Datang <b>{{ Auth::user()->name }}</b>.</h4>
        <hr class="mt-0 mb-4">
        <div class="row">
            <div class="col-lg-6 mb-6">
                <!-- Transaction card 1-->
                <div class="card h-100 border-start-lg border-start-primary">
                    <div class="card-body">
                        <div class="small text-muted">Current Balance</div>
                        <div class="h3">{{ $balance->amount }}</div>
                        <p class="text-arrow-icon small">
                            Saldo anda
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-6">
                <!-- Transaction card 2-->
                <div class="card h-100 border-start-lg border-start-secondary">
                    <div class="card-body">
                        <div class="small text-muted">Add Transaction</div>
                        <div class="h3">Topup or Transfer</div>
                        <a class="text-arrow-icon small text-secondary" href="/transaction">
                            Click here
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right">
                                <line x1="5" y1="12" x2="19" y2="12"></line>
                                <polyline points="12 5 19 12 12 19"></polyline>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Transaction history card-->
        <div class="card mt-4 mb-4">
            <div class="card-header">Transaction History</div>
            <div class="card-body p-0">
                <div class="row mx-0 justify-content-between">
                    <div class="col-3">
                        <!-- Filter dropdown -->
                        <div class="form-group mt-2 mb-2">
                            <select onchange="window.location.href = '/filter/' + this.value;">
                                <option value="">All</option>
                                <option value="topup" {{ session()->get('filter') == 'topup' ? 'selected' : '' }}>Top Up</option>
                                <option value="transaction" {{ session()->get('filter') == 'transaction' ? 'selected' : '' }}>Transaction</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <!-- Search bar -->
                        <div class="form-group mt-2 mb-2">
                            <form action="/search" method="GET">
                                <input type="text" name="query" placeholder="Search" value="{{ session()->get('query') }}">
                                <button type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Transaction history table-->
                <div class="table-responsive table-transaction-history">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th class="border-gray-200" scope="col">Amount</th>
                                <th class="border-gray-200" scope="col">Type</th>
                                <th class="border-gray-200" scope="col">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($records as $record)
                                <tr>
                                    <td>{{ $record->amount }}</td>
                                    <td>{{ $record->type }}</td>
                                    <td>{{ $record->desc }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <!-- Pagination links -->
                    {{ $records->links() }}

                </div>
            </div>
        </div>
    </div>

    <style>
        body {
            margin-top: 20px;
            background-color: #f2f6fc;
            color: #69707a;
        }

        .img-account-profile {
            height: 10rem;
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .card {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(33 40 50 / 15%);
        }

        .card .card-header {
            font-weight: 500;
        }

        .card-header:first-child {
            border-radius: 0.35rem 0.35rem 0 0;
        }

        .card-header {
            padding: 1rem 1.35rem;
            margin-bottom: 0;
            background-color: rgba(33, 40, 50, 0.03);
            border-bottom: 1px solid rgba(33, 40, 50, 0.125);
        }

        .form-control,
        .dataTable-input {
            display: block;
            width: 100%;
            padding: 0.875rem 1.125rem;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1;
            color: #69707a;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #c5ccd6;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.35rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .nav-borders .nav-link.active {
            color: #0061f2;
            border-bottom-color: #0061f2;
        }

        .nav-borders .nav-link {
            color: #69707a;
            border-bottom-width: 0.125rem;
            border-bottom-style: solid;
            border-bottom-color: transparent;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            padding-left: 0;
            padding-right: 0;
            margin-left: 1rem;
            margin-right: 1rem;
        }

        .fa-2x {
            font-size: 2em;
        }

        .table-transaction-history th,
        .table-transaction-history td {
            padding-top: 0.75rem;
            padding-bottom: 0.75rem;
            padding-left: 1.375rem;
            padding-right: 1.375rem;
        }

        .table> :not(caption)>*>*,
        .dataTable-table> :not(caption)>*>* {
            padding: 0.75rem 0.75rem;
            background-color: var(--bs-table-bg);
            border-bottom-width: 1px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        }

        .border-start-primary {
            border-left-color: #0061f2 !important;
        }

        .border-start-secondary {
            border-left-color: #6900c7 !important;
        }

        .border-start-success {
            border-left-color: #00ac69 !important;
        }

        .border-start-lg {
            border-left-width: 0.25rem !important;
        }

        .h-100 {
            height: 100% !important;
        }
    </style>
@endsection
