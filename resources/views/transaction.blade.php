@extends('layout')

@section('konten')
    <div class="container-xl px-4 mt-4 mb-4">
        <h2 class="text-center">Add Transaction</h2>
        <hr>
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <form action="{{ route('actiontransaction') }}" method="post">
            @csrf
            <div class="form-group mb-2">
                <label>Amount</label>
                <input type="number" name="amount" class="form-control" placeholder="Amount" required="" min="0" step="1">
            </div>
            <div class="form-group mb-2">
                <label>Type</label>
                <select class="form-select" name="type" required>
                    <option value="" selected>Select type</option>
                    <option value="topup">Top Up</option>
                    <option value="transaction">Transfer</option>
                </select>
            </div>
            <div class="form-group mb-2">
                <label>Description</label>
                <input type="text" name="desc" class="form-control" placeholder="Desc" required>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </form>
    </div>
@endsection
